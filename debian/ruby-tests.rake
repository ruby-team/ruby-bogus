require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList.new('spec/**/*_spec.rb') do |fl|
    fl.exclude 'spec/bogus/fakes/fake_ar_attributes_spec.rb'
    fl.exclude 'spec/bogus/ruby_2_support_spec.rb' unless RUBY_VERSION < '3'
    fl.exclude 'spec/bogus/ruby_2_1_support_spec.rb' unless RUBY_VERSION < '3'
  end
end
